USE Northwind
GO
DROP DATABASE ConfConf2
GO
CREATE DATABASE ConfConf2
GO
USE ConfConf2


CREATE TABLE Country(
	CountryID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Country varchar(50) UNIQUE NOT NULL,
)

CREATE TABLE City(
	CityID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	City varchar(50) UNIQUE NOT NULL,
	CountryID int FOREIGN KEY REFERENCES Country(CountryID) NOT NULL,
	PostalCode char(6) UNIQUE NOT NULL CHECK(PostalCode like '[0-9][0-9][0-9][0-9][0-9]')
)

CREATE TABLE Company(
	CompanyID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Address varchar(50) UNIQUE NOT NULL,
	CityID int FOREIGN KEY REFERENCES City(CityID)  NOT NULL,
	Phone varchar(50) UNIQUE NOT NULL,
	Email varchar(50) UNIQUE NOT NULL,
	NIP char(10) UNIQUE NOT NULL,
	CompanyName varchar(50) UNIQUE NOT NULL	
)

CREATE TABLE Participant(
	ParticipantID int IDENTITY(1,1) PRIMARY KEY NOT NULL
)

CREATE TABLE Individual(
	PersonID int PRIMARY KEY REFERENCES Participant(ParticipantID) NOT NULL,
	Address varchar(50) NOT NULL,
	CityID int FOREIGN KEY REFERENCES City(CityID)  NOT NULL,
	Phone varchar(15) UNIQUE NULL,
	Pesel char(11) UNIQUE NOT NULL,
	Name varchar(30) NOT NULL,
	Surname varchar(30) NOT NULL,
	Discount varchar(30) NULL CHECK(Discount = 'Student' or Discount = NULL),
	Email varchar(50) UNIQUE NULL CHECK(Email like '%_@_%._%')
)

CREATE TABLE Employee(
	EmployeeID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Name varchar(50) NOT NULL,
	Surname varchar(50) NOT NULL,
	Address varchar(50) NOT NULL,
	Phone varchar(50) UNIQUE NULL,
	CityID int FOREIGN KEY REFERENCES City(CityID)  NOT NULL
)

 
CREATE TABLE Conference(
	ConferenceID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	ConferenceName varchar(250) NOT NULL,
	StartDate date NOT NULL,
	EndDate date NOT NULL,
	Cost money NOT NULL CHECK(Cost > 0),
	Capacity smallint NOT NULL CHECK(Capacity > 0),
	EmployeeID int FOREIGN KEY REFERENCES Employee(EmployeeID) NOT NULL,
	StudentsDiscount float NULL  CHECK(StudentsDiscount >= 0 AND StudentsDiscount <= 1),
	Active bit NOT NULL
)

CREATE TABLE Pricing(
	PricingID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	ConferenceID int FOREIGN KEY REFERENCES Conference(ConferenceID) NOT NULL,
	Reduction float NOT NULL CHECK(Reduction >= 0 AND Reduction <= 1),
	Deadline date NOT NULL
)

CREATE TABLE ConferenceDay(
	ConferenceID int FOREIGN KEY REFERENCES Conference(ConferenceID) NOT NULL,
	ConferenceDayID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Date date NOT NULL
)

CREATE TABLE CompanyWorker(
	Surname varchar(50) NULL,
	Name varchar(50) NULL,
	Discount varchar(50) NULL CHECK(Discount = 'Student' or Discount = NULL),
	Pesel char(11) NULL,
	ParticipantID int PRIMARY KEY REFERENCES Participant(ParticipantID)  NOT NULL,
	CompanyID int FOREIGN KEY REFERENCES Company(CompanyID) NOT NULL
)

CREATE TABLE Orders(
	OrderID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	CompanyID int FOREIGN KEY REFERENCES Company(CompanyID) NULL,
	PersonID int FOREIGN KEY REFERENCES Individual(PersonID) NULL,
	ConferenceID int FOREIGN KEY REFERENCES Conference(ConferenceID) NOT NULL,
	OrderDate date NOT NULL
)

CREATE TABLE OrderDetails(
	OrderID int FOREIGN KEY REFERENCES Orders(OrderID) NOT NULL,
	ConferenceDayID int FOREIGN KEY REFERENCES ConferenceDay(ConferenceDayID) NOT NULL,
	Spots int NOT NULL CHECK(Spots > 0),
)

CREATE TABLE DayReservation(
	DayReservationID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	ParticipantID int FOREIGN KEY REFERENCES Participant(ParticipantID) NOT NULL,
	ConferenceDayID int FOREIGN KEY REFERENCES ConferenceDay(ConferenceDayID) NOT NULL,
	OrderID int FOREIGN KEY REFERENCES Orders(OrderID) NOT NULL,
)

CREATE TABLE Workshop(
	WorkshopID int IDENTITY(1,1) PRIMARY KEY  NOT NULL,
	ConferenceDayID int FOREIGN KEY REFERENCES ConferenceDay(ConferenceDayID) NOT NULL,
	WorkshopName varchar(250) NOT NULL,
	Capacity smallint NOT NULL CHECK(Capacity > 0),
	Price money NULL CHECK(Price >=0),
	StartHour time NOT NULL,
	EndHour time NOT NULL,
)

CREATE TABLE Payment(
	OrderID int FOREIGN KEY REFERENCES Orders(OrderID) NOT NULL,
	MoneyPaid money NOT NULL CHECK(MoneyPaid > 0),
	PaymentID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	PaymentDate date NOT NULL, 
)

CREATE TABLE WorkshopReservation(
	WorkshopReservationID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	DayReservationID int FOREIGN KEY REFERENCES DayReservation(DayReservationID) NOT NULL,
	WorkshopID int FOREIGN KEY REFERENCES Workshop(WorkshopID) NOT NULL,
)

ALTER TABLE dbo.OrderDetails
ADD
CONSTRAINT PK_OrderDetails
	PRIMARY KEY NONCLUSTERED (OrderID,ConferenceDayID)
GO

-- ===================================== PROCEDURY ==================================
-- Maciej Lobodznski
-- ==================================================================================

-- ==========================================
-- procedura new_country, dodaje nowe państwo
-- ==========================================

CREATE PROCEDURE new_country
	-- parametry
	@Country varchar(50)

AS 
BEGIN

	SET NOCOUNT ON;
		INSERT INTO Country
		VALUES(@Country)
		
		
END
GO

-- ========================================
-- procedura new_city , dodaje nowe miasto
-- ========================================

CREATE PROCEDURE new_city
	--parametry
	@City varchar(50),
	@PostalCode char(6),
	@Country varchar(50)

AS
BEGIN

	SET NOCOUNT ON;
		declare @CountryID as int;
		IF (select City from City where City = @City) IS NULL
			BEGIN
			IF (select Country from Country where Country = @Country) IS NULL
				BEGIN
				EXECUTE new_country @Country;
				set @CountryID = @@IDENTITY;
				END
			ELSE
				BEGIN
				set @CountryID = (select CountryID from Country where Country = @Country)
				END
			INSERT INTO City
			VALUES(@City,@CountryID,@PostalCode);
			END
		ELSE
			BEGIN
			SELECT CityID from City where City = @City
			END
END
GO

-- ===============================================================
-- procedura new_ind_client, dodaje nowego klienta indywidualnego
-- ===============================================================

CREATE PROCEDURE new_ind_client
	-- parametry
	@Address varchar(50),
	@Country varchar(50),
	@City varchar(50),
	@PostalCode char(6),
	@Phone varchar(15) = null,
	@Pesel char(11),
	@Name varchar(30),
	@Surname varchar(30),
	@Discount varchar(30),
	@Email varchar(50) = null 

AS
BEGIN

	SET NOCOUNT ON;
		declare @CityID as int;
		declare @ParticipantID as int
		IF (select City from City where City = @City) IS NULL
		BEGIN
			EXECUTE new_city @City,@PostalCode,@Country;
			set @CityID = @@IDENTITY;
		END
		ELSE 
			BEGIN
			set @CityID = (select CityID from City where City = @City);
			END

		EXECUTE new_participant ;
		set @ParticipantID = @@IDENTITY;

		INSERT INTO Individual
		values(@ParticipantID,@Address,@CityID,@Phone,@Pesel,@Name,@Surname,@Discount,@Email)
END
GO

-- ==========================================================
-- procedura new_com_client, dodaje nowego klienta firmowego
-- ==========================================================

CREATE PROCEDURE new_com_client
-- parametry
	@Adress varchar(50),
	@Country varchar(50),
	@City varchar(50),
	@PostalCode char(6),
	@Phone varchar(15) = null,
	@Email varchar(50) = null,
	@NIP char(10),
	@CompanyName varchar(30)
	 

AS
BEGIN

	SET NOCOUNT ON;
		declare @CityID as int;
		IF (select City from City where City = @City) IS NULL
		BEGIN
			EXECUTE new_city @City,@PostalCode,@Country;
			set @CityID = @@IDENTITY;
		END
		ELSE 
			BEGIN
			set @CityID = (select CityID from City where City = @City);
			END
		INSERT INTO Company
		values(@Adress,@CityID,@Phone,@Email,@NIP,@CompanyName)
END
GO

-- =======================================================
-- procedura new_com_work, dodaje nowego pracownika firmy
-- =======================================================

CREATE PROCEDURE new_com_work
-- parametry
	@Name varchar(50) = NULL,
	@Surname varchar(50) = NULL,
	@Discount varchar(30) = NULL,
	@Pesel varchar(50) = NULL,
	@CompanyID int

AS
BEGIN

	SET NOCOUNT ON;
		declare @ParticipantID as int;
		EXECUTE new_participant ;
		SET @ParticipantID = @@IDENTITY
		INSERT INTO CompanyWorker
		VALUES(@Name,@Surname,@Discount,@Pesel,@ParticipantID,@CompanyID)
END
GO

-- ===============================================================
-- procedura new_participant, dodaje nowego uczestnika konferencji
-- ===============================================================

CREATE PROCEDURE new_participant

AS
BEGIN

	SET NOCOUNT ON;
		INSERT INTO Participant
		DEFAULT VALUES;
END
GO

-- ===============================================================
-- procedura new_conf, dodaje nowa konferencje:
--	tworzy wszystkie dni konferencji,
--	tworzy pierwszy próg cenowy
-- ===============================================================

CREATE PROCEDURE new_conf 
-- parametry
	@ConferenceName varchar(250),
	@StartDate date,
	@EndDate date,
	@Cost money,
	@Capacity smallint,
	@EmployeeID int,
	@StudentDiscount float,
	@Active bit = 1

AS
BEGIN

	SET NOCOUNT ON;
		declare @ConferenceID as int;
		INSERT INTO Conference
		VALUES(@ConferenceName,@StartDate,@EndDate,@Cost,@Capacity,@EmployeeID,@StudentDiscount,@Active);
		SET @ConferenceID = @@IDENTITY;

		EXECUTE new_pricing @ConferenceID,1,@StartDate;

		WHILE (@StartDate != (DATEADD(day,1,@EndDate)))
			BEGIN
			EXECUTE new_conf_day @ConferenceID,@StartDate;
			SET @StartDate = DATEADD(day,1,@StartDate)
			END
		
END
GO

-- =================================================
-- procedura new_conf_day, dodaje dzien konferencji
-- =================================================

CREATE PROCEDURE new_conf_day
-- parametry
	@ConferenceID int,
	@Date date

AS
BEGIN

	SET NOCOUNT ON;
		INSERT INTO ConferenceDay
		VALUES(@ConferenceID,@Date)
END
GO

-- =============================================
-- procedura new_workshop, dodaje nowy warsztat
-- =============================================

CREATE PROCEDURE new_workshop
-- parametry
	@ConferenceDayID int,
	@WorkshopName varchar(250),
	@Capacity smallint,
	@Price money,
	@StartHour time,
	@EndHour time
AS
BEGIN

	SET NOCOUNT ON;
		INSERT INTO Workshop
		VALUES(@ConferenceDayID,@WorkshopName,@Capacity,@Price,@StartHour,@EndHour)
END
GO

-- ============================================
-- procedura new_order, dodaje nowy zamowienie
-- ============================================

CREATE PROCEDURE new_order
-- parametry
	@CompanyID int = NULL,
	@PersonID int = NULL,
	@ConferenceID int,
	@OrderDate date

AS
BEGIN

	SET NOCOUNT ON;
		INSERT INTO Orders
		VALUES(@CompanyID,@PersonID,@ConferenceID,@OrderDate)
END
GO

-- ====================================================================
-- procedura new_day_order, dodaje nowe zamowienie na dzien konferencji
-- ====================================================================

CREATE PROCEDURE new_day_order
-- parametry
	@ConferenceDayID int,
	@Spots int,
	@OrderID int

AS
BEGIN
	
	SET NOCOUNT ON;
		DECLARE @Company as int 
		DECLARE @ParticipantID as int
		DECLARE @Lopp as int

		SET @Lopp = @Spots
		SET @Company = (SELECT CompanyID FROM Orders WHERE OrderID = @OrderID)
		IF (((SELECT count(*) from OrderDetails where OrderID = @OrderID and ConferenceDayID = @ConferenceDayID) = 0) and ((SELECT PersonID FROM Orders WHERE OrderID = @OrderID) IS NULL))
			BEGIN
			WHILE @Lopp > 0 
				BEGIN
				EXECUTE new_com_work @CompanyID = @Company;
				SET @ParticipantID = (select top 1 ParticipantID from CompanyWorker order by ParticipantID DESC)
				EXECUTE new_day_res @ParticipantID,@ConferenceDayID,@OrderID;
				SET @Lopp = @Lopp - 1;
				END
			END
		INSERT INTO OrderDetails
		VALUES(@OrderID,@ConferenceDayID,@Spots)
END
GO

-- ============================================
-- procedura new_payment, dodaje nowa platnosc
-- ============================================

CREATE PROCEDURE new_payment
-- parametry
	@OrderID int,
	@AmountPaid money,
	@PaymentDate date

AS
BEGIN

	SET NOCOUNT ON;
		INSERT INTO Payment
		VALUES(@OrderID,@AmountPaid,@PaymentDate)
END
GO

-- ==========================================
-- procedura new_employee, dodaje pracownika
-- ==========================================

CREATE PROCEDURE new_employee
-- parametry
	@Name varchar(50),
	@Surnane varchar(50),
	@Address varchar(50),
	@Phone varchar(15),
	@Country varchar(50),
	@City varchar(50),
	@PostalCode char(6)
	
AS
BEGIN
	
	SET NOCOUNT ON;
	declare @CityID as int;
	IF (select City from City where City = @City) IS NULL
		BEGIN
			EXECUTE new_city @City,@PostalCode,@Country;
			set @CityID = @@IDENTITY;
		END
		ELSE 
			BEGIN
			set @CityID = (select CityID from City where City = @City);
			END

		INSERT INTO Employee
		VALUES(@Name,@Surnane,@Address,@Phone,@CityID)
END
GO

-- ===========================================================
-- procedura new_pricing, ktora dodaje nowy stopien platnosci
-- ===========================================================

CREATE PROCEDURE new_pricing
-- parametry
	@ConferenceID int,
	@Reduction float,
	@Deadline date

AS
BEGIN

	SET NOCOUNT ON;
		INSERT INTO Pricing
		VALUES(@ConferenceID,@Reduction,@Deadline)
END
GO

-- ======================================================
-- procedura new_day_res, dodaje nowa rezerwacje dzienna
-- ======================================================

CREATE PROCEDURE new_day_res
-- parametry
	@ParicipantID int,
	@ConferenceDayID int,
	@OrderID int

AS
BEGIN

	SET NOCOUNT ON;
		INSERT INTO DayReservation
		VALUES(@ParicipantID,@ConferenceDayID,@OrderID)
END
GO

-- ============================================================
-- procedura update_com_work, uzupełnia dane pracownika
-- ============================================================

CREATE PROCEDURE update_com_work
-- parametry
	@ParticiapntID int,
	@Surname varchar(50),
	@Name varchar(50),
	@Discount varchar(50) = NULL,
	@Pesel char(11)

AS
BEGIN
	SET NOCOUNT ON;
		UPDATE CompanyWorker
		SET Surname=@Surname, Name=@Name, Discount=@Discount,Pesel=@Pesel
		WHERE ParticipantID = @ParticiapntID
END
GO

-- ============================================================
-- procedura new_work_res, dodaje nowej rezerwacji na warsztat
-- ============================================================

CREATE PROCEDURE new_work_res
-- parametry
	@DayReservationID int,
	@WorkshopID int

AS
BEGIN

	SET NOCOUNT ON;
		INSERT INTO WorkshopReservation
		VALUES(@DayReservationID,@WorkshopID)
END
GO

-- =======================================================================
-- procedura chan_conf_state, zmienia stan aktywności rezerwacji na podany
-- =======================================================================

CREATE PROCEDURE chan_conf_state
-- parametry
	@ConferenceID int,
	@State bit

AS
BEGIN
	
	SET NOCOUNT ON;
	UPDATE Conference
	SET Active = @State
	WHERE Conference.ConferenceID = @ConferenceID

END
GO

-- ===============================================================================
-- procedura delete_participant, usuwa wszystkie zapisy uczestnika na konferencje, 
-- warsztaty oraz jego odpowiednik w CompanyWorker,
-- jezeli jest to klient indywidualny to nie usuwamy go z tabeli Participant
-- ===============================================================================

CREATE PROCEDURE delete_participant
-- parametry
	@ParticipantID int
AS
BEGIN
	SET NOCOUNT ON;
		DELETE FROM WorkshopReservation WHERE WorkshopReservation.DayReservationID
		in (SELECT DayReservationID from DayReservation where ParticipantID = @ParticipantID)

		DELETE FROM DayReservation WHERE DayReservation.ParticipantID = @ParticipantID		
		
		IF EXISTS (SELECT ParticipantID FROM CompanyWorker WHERE ParticipantID = @ParticipantID)
			BEGIN
			DELETE FROM Participant WHERE ParticipantID = @ParticipantID
			DELETE FROM CompanyWorker WHERE ParticipantID = @ParticipantID
			END
END
GO

-- ===============================================================================
-- procedura delete_order, usuwa wszystkich uczestnikow z danego zamowienia,
-- historie platnosci i samo zamowienie  
-- ===============================================================================

CREATE PROCEDURE delete_order
-- parametry
	@OrderID int

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @Participant as int

	DELETE FROM WorkshopReservation WHERE WorkshopReservation.DayReservationID
		in (SELECT DayReservationID from DayReservation where OrderID = @OrderID)

	DELETE FROM DayReservation WHERE DayReservation.OrderID = @OrderID
	
	IF (Select PersonID from Orders where OrderID = @OrderID) IS NULL
	BEGIN 
	WHILE EXISTS (SELECT Participant.ParticipantID from Participant inner join DayReservation on Participant.ParticipantID	= DayReservation.ParticipantID where DayReservation.OrderID = @OrderID)
		BEGIN 
		SET @Participant = (SELECT TOP 1  Participant.ParticipantID from Participant inner join DayReservation on Participant.ParticipantID = DayReservation.ParticipantID where OrderID = @OrderID)
		EXECUTE delete_participant @Participant
		END
	END

	DELETE FROM OrderDetails WHERE OrderDetails.OrderID = @OrderID

	DELETE FROM Orders WHERE OrderID = @OrderID
END
GO

-- =======================================================================================
-- Procedura show_conf_participants, która pokazuje uczestników danego dnia konferencji
-- =======================================================================================

CREATE PROCEDURE show_conf_participants
-- paramtery
	@ConferenceDayID int

AS
BEGIN

	SET NOCOUNT ON;
	SELECT Name,Surname FROM dbo.ShowConfDayParticipant WHERE ConferenceDayID = @ConferenceDayID
END
GO

-- =================================================================================================
-- Procedura show_loyal, która pokazuje uczestników, którzy więcej niż X razy byli na konferencjach
-- =================================================================================================

CREATE PROCEDURE show_loyal
-- parametry
	@X int 

AS 
BEGIN

	SET NOCOUNT ON;
	SELECT Name,Surname FROM dbo.ShowConfParticipant WHERE [Ilosc konferencji] >= @X
END
GO 


-- ===================================== WIDOKI =====================================
-- Maciej Lobodzinski
-- ==================================================================================

-- =============================================================================
-- Widok pokazujący dane wszystkich klientów - zarówno firm jak i indywidualnych
-- =============================================================================

CREATE VIEW ShowClients AS
SELECT  Name+' '+Surname as [Nazwa],Address,City.City,Country.Country,Phone,Email,Pesel,'Klient Indywidualny' as [Klient] FROM Individual
inner join City on City.CityID = Individual.CityID
inner join Country on Country.CountryID = City.CountryID
UNION
SELECT CompanyName,Address,City,Country,Phone,Email,NIP,'Firma' as [Klient] FROM Company
inner join City on City.CityID = Company.CityID
inner join Country on Country.CountryID = City.CountryID
GO

-- =============================================================================
-- Widok pokazujący wszystkich uczestników wszystkich konferencji
-- =============================================================================

CREATE VIEW ShowConfParticipant AS
SELECT Name,Surname,COUNT(DayReservationID) as [Ilosc konferencji] FROM Individual 
INNER JOIN Participant ON Participant.ParticipantID = Individual.PersonID
INNER JOIN DayReservation on Participant.ParticipantID = DayReservation.ParticipantID
GROUP BY Name,Surname 
UNION ALL
SELECT Name,Surname,COUNT(DayReservationID) as [Ilosc konferencji] FROM CompanyWorker 
INNER JOIN Participant ON Participant.ParticipantID = CompanyWorker.ParticipantID
INNER JOIN DayReservation on Participant.ParticipantID = DayReservation.ParticipantID
GROUP BY Name,Surname
GO

-- =============================================================================
-- Widok pokazujący wszystkich uczestników konferencji z podziałem na zamówienia
-- =============================================================================

create VIEW ShowOrderParticipants AS
SELECT OrderID,ConferenceDayID,Name,Surname,Discount from Individual 
inner join Participant on Participant.ParticipantID = Individual.PersonID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
UNION ALL
SELECT OrderID,ConferenceDayID,Name,Surname,Discount from CompanyWorker 
inner join Participant on CompanyWorker.ParticipantID = Participant.ParticipantID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
GO

-- =============================================================================
-- Widok pokazujący wszystkich uczestników konferencji z podziałem na zamówienia
-- =============================================================================

create VIEW ShowWorkshopOrderParticipants AS
SELECT OrderID,count(*) as [spots],WorkshopID from Individual 
inner join Participant on Participant.ParticipantID = Individual.PersonID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
left outer join WorkshopReservation on DayReservation.DayReservationID = WorkshopReservation.DayReservationID
group by OrderID,WorkshopID
UNION ALL
SELECT OrderID,count(*) as [spots],WorkshopID from CompanyWorker 
inner join Participant on CompanyWorker.ParticipantID = Participant.ParticipantID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
left outer join WorkshopReservation on DayReservation.DayReservationID = WorkshopReservation.DayReservationID
group by OrderID,WorkshopID
GO

-- =============================================================================
-- Widok pokazujący wszystkich uczestników z podziałem na dzień konferencji
-- =============================================================================

CREATE VIEW ShowConfDayParticipants AS
SELECT ConferenceDayID,Name,Surname,PersonID FROM Individual
inner join Participant on Participant.ParticipantID = Individual.PersonID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
GROUP BY ConferenceDayID,Name,Surname,PersonID
UNION ALL
SELECT ConferenceDayID,Name,Surname,CompanyWorker.ParticipantID FROM CompanyWorker
inner join Participant on Participant.ParticipantID = CompanyWorker.ParticipantID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
GO 

-- =============================================================================
-- Widok pokazujący wszystkich uczestników z podziałem na Warsztaty
-- =============================================================================

CREATE VIEW ShowWorkshopParticipants AS
SELECT WorkshopID,Name,Surname,PersonID FROM Individual
inner join Participant on Participant.ParticipantID = Individual.PersonID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
inner join WorkshopReservation on WorkshopReservation.DayReservationID = DayReservation.DayReservationID
GROUP BY WorkshopID,Name,Surname,PersonID
UNION ALL
SELECT WorkshopID,Name,Surname,CompanyWorker.ParticipantID FROM CompanyWorker
inner join Participant on Participant.ParticipantID = CompanyWorker.ParticipantID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
inner join WorkshopReservation on WorkshopReservation.DayReserv
ationID = DayReservation.DayReservationID
GROUP BY WorkshopID,Name,Surname,CompanyWorker.ParticipantID
GO

-- ====================================================================================================
-- Widok pokazujący wszystkie aktywne konferencje na które wciąż są wolne miejsca z ilością tych miejsc
-- ====================================================================================================

CREATE VIEW ShowAvailibleConf AS
SELECT Conference.ConferenceID,ConferenceDayID,dbo.ConferenceDayFreeSpots(ConferenceDayID) as [Wolne miejsca] from Conference
inner join ConferenceDay on ConferenceDay.ConferenceID = Conference.ConferenceID
where dbo.ConferenceDayFreeSpots(ConferenceDayID) > 0
GROUP BY Conference.ConferenceID,ConferenceDayID
GO

-- ==========================================================
-- Widok pokazujący wszystkie niezapłacone jeszcze zamówienia
-- ==========================================================

CREATE VIEW ShowNotPaidOrders AS
SELECT OrderID,PersonID as [Klient indywidualny],CompanyID as [Firma], dbo.AmountPaid(OrderID) as [Kwota wplacona], (dbo.OrderValue(OrderID)-dbo.AmountPaid(OrderID)) as [Pozostalo do zaplaty],
DATEDIFF(day,OrderDate,GETDATE()) as [Pozostalo dni aby zaplacic calosc] FROM Orders GROUP BY OrderID,PersonID,CompanyID,OrderDate
GO

-- ==============================================================================================
-- Widok pokazujacy te firmy, ktore nie uzupelnily jeszcze list i ile dni zostalo do uzupelnienia
-- ==============================================================================================

CREATE VIEW ShowNotEnlisted AS
SELECT DISTINCT CompanyName, DATEDIFF(DAY,GETDATE(),Conference.StartDate) as [Pozostalo dni] FROM Company 
inner join CompanyWorker on Company.CompanyID = CompanyWorker.CompanyID
inner join Participant on CompanyWorker.ParticipantID = Participant.ParticipantID
inner join DayReservation on DayReservation.ParticipantID = Participant.ParticipantID
inner join ConferenceDay on ConferenceDay.ConferenceDayID = DayReservation.ConferenceDayID
inner join Conference on Conference.ConferenceID = ConferenceDay.ConferenceID
where CompanyWorker.Name IS NULL
GO 

-- ===================================== FUNKCJE ====================================
-- Maciej Lobodzinski
-- ==================================================================================

-- ========================================================================================
-- Funkcja zwracajaca warsztaty dla danej rezerwacji, które odbywają się w tym samym czasie
-- ========================================================================================

CREATE FUNCTION WorkshopsInTheSameTime(@DayReservationID int,@WorkshopID int)
	RETURNS int
	AS
	BEGIN
		DECLARE @Start as time
		DECLARE @End as time
		DECLARE @Flag as int

		SET @Start = (SELECT StartHour FROM Workshop where WorkshopID = @WorkshopID)
		SET @End = (SELECT EndHour FROM Workshop where WorkshopID = @WorkshopID)
		SET @Flag = (SELECT count(*) FROM WorkshopReservation inner join Workshop on WorkshopReservation.WorkshopID = Workshop.WorkshopID
					where WorkshopReservation.DayReservationID = @DayReservationID	and 
					dbo.SameTimeHour(@Start,@End,Workshop.StartHour,Workshop.EndHour) = 1)

		RETURN @Flag
	END
GO

-- ==============================================================================
-- Funkcja zwracajaca 1 jezeli przedzialy podanych godzin sie nachodza
-- ==============================================================================

CREATE FUNCTION SameTimeHour(@Start1 time,@End1 time,@Start2 time,@End2 time)
	RETURNS bit
	AS
	BEGIN
		DECLARE @Flag as bit
		SET @Flag  = 0

		IF ((@Start1 > @Start2) and (@Start1 < @End2)) or ((@End1 < @End2) and (@End1 > @Start2)) or ((@Start1 <= @Start2) and (@End1 >= @End2))
			BEGIN
			SET @Flag = 1
			END
		RETURN @Flag
	END
GO

-- =============================================================================
-- Funkcja zwracająca ilość wolnych miejsc na dany dzien konferencji
-- =============================================================================

CREATE FUNCTION  ConferenceDayFreeSpots(@ConferenceDayID int)
	RETURNS smallint
	AS
	BEGIN
		DECLARE @Total as smallint
		DECLARE @Taken as smallint

		SET @Total = (
			select Capacity from Conference 
			where Conference.ConferenceID = (select ConferenceID from ConferenceDay where ConferenceDayID = @ConferenceDayID)
			)
		SET @Taken = (
			select ISNULL(SUM(x),0) from
			(
				select OrderDetails.Spots as x from OrderDetails 
				where OrderDetails.ConferenceDayID = @ConferenceDayID
			) as x)
		
		RETURN @Total - @Taken

	END
GO

-- SELECT dbo.ConferenceDayFreeSpots(1)

-- =============================================================================
-- Funkcja zwracająca calkowity koszt danego zamowienia + warsztaty
-- =============================================================================

CREATE FUNCTION OrderValue(@OrderID int)
	RETURNS smallint
	AS
	BEGIN
		DECLARE @Total as smallint
		DECLARE @Discount as float
		DECLARE @Participants as int
		DECLARE @Students as int
		DECLARE @Price as money
		DECLARE @Conference as int
		DECLARE @Date as date
		DECLARE @Workshops as money
		SET @Conference = (select ConferenceID from Orders where Orders.OrderID = @OrderID)			
		SET @Date = (select OrderDate from Orders where Orders.OrderID = @OrderID)
		SET @Discount = (select StudentsDiscount from Conference where ConferenceID = (select ConferenceID from Orders where @OrderID = OrderID))
		SET @Participants = (select SUM(x) from (select Spots as x from OrderDetails where OrderDetails.OrderID = @OrderID) as x)
		SET @Price = (select top 1 Conference.Cost*Pricing.Reduction from Conference 
					inner join Pricing on Pricing.ConferenceID = Conference.ConferenceID and Conference.ConferenceID = @Conference 
					where Pricing.Deadline > @Date order by Pricing.Deadline) 
		SET @Students =  (select ISNULL(COUNT(*),0) from (select * from ShowOrderParticipants where ShowOrderParticipants.OrderID = @OrderID and ShowOrderParticipants.Discount = 'Student') as [Studenci])
		
		SET @Workshops = (select SUM(x) from (select ISNULL(Price,0) as x from WorkshopReservation 
						inner join Workshop on Workshop.WorkshopID = WorkshopReservation.WorkshopID 
						inner join DayReservation on DayReservation.OrderID = @OrderID) as x)

		SET @Total = @Price*(@Participants - @Students) + @Price*(1-@Discount)*(@Students) + @Workshops
	RETURN ISNULL(@Total,0)
	END
GO

-- ================================================================
-- Funkcja zwracająca koszt zamówienia dla jednego dnia konferencji
-- ================================================================

CREATE FUNCTION ConfDayCost(@ConferenceDayID int, @OrderID int)
	RETURNS money
	AS
	BEGIN

	DECLARE @Cost money
	DECLARE @Participants int
	DECLARE @Students int
	DECLARE @Discount float
	DECLARE @Conference as int
	DECLARE @Date as date
	
	SET @Conference = (select ConferenceID from Orders where Orders.OrderID = @OrderID)			
	
	SET @Date = (select OrderDate from Orders where Orders.OrderID = @OrderID)

	SET @Cost = (select top 1 Conference.Cost*Pricing.Reduction from Conference 
					inner join Pricing on Pricing.ConferenceID = Conference.ConferenceID and Conference.ConferenceID = @Conference 
					where Pricing.Deadline > @Date order by Pricing.Deadline)

	SET @Participants = (select count(*) from dbo.ShowOrderParticipants where ConferenceDayID = @ConferenceDayID and OrderID = @OrderID)

	SET @Students = (select count(*) from dbo.ShowOrderParticipants where ConferenceDayID = @ConferenceDayID and OrderID = @OrderID and Discount = 'Student') 
	
	SET @Discount = (select StudentsDiscount from Conference inner join ConferenceDay on Conference.ConferenceID = ConferenceDay.ConferenceID where ConferenceDayID = @ConferenceDayID)


	RETURN ((@Participants-@Students)*@Cost + @Students*(1-@Discount)*@Cost)
	
	END
GO




-- =============================================================================
-- Funkcja zwracająca kwotę wpłaconą dla danego zamówienia
-- =============================================================================

CREATE FUNCTION AmountPaid(@OrderID int)
	RETURNS money
	AS
	BEGIN
		DECLARE @Total as money

		SET @Total = (select ISNULL(SUM(x),0) from (select MoneyPaid as x from Payment where OrderID = @OrderID) as x)

		RETURN @Total
	END
GO

-- =============================================================================
-- Funkcja zwracająca ilość pustych miejsc na warsztat
-- =============================================================================

CREATE FUNCTION WorkshopFreeSpots(@WorkshopID int)
	RETURNS INT
	AS
	BEGIN
		DECLARE @Total as int

		SET @Total = (select Workshop.Capacity - (select COUNT(*) from WorkshopReservation where WorkshopReservation.WorkshopID = @WorkshopID) as x from Workshop)

		RETURN @Total
	END
GO



-- ===================================== TRIGGERY ===================================
-- Maciej Lobodzinski
-- ==================================================================================

CREATE TRIGGER WorkshopSameHour on WorkshopReservation
INSTEAD OF INSERT
AS 
	BEGIN
		DECLARE @DayReservationID as int
		DECLARE @WorkshopID as int

		SET @DayReservationID = (SELECT DayReservationID from inserted)
		SET @WorkshopID = (SELECT WorkshopID from inserted)
		IF (dbo.WorkshopsInTheSameTime(@DayReservationID,@WorkshopID) > 0)
					BEGIN
					RAISERROR('Nie mozesz zapisac sie na warsztaty w tych samych godzinach!',1,1)
					ROLLBACK TRANSACTION
					END	
		ELSE
			INSERT INTO WorkshopReservation
			VALUES(@DayReservationID,@WorkshopID)
	END
GO


-- ==================================================================================
-- Trigger pilnujacy dat w tabeli Conference
-- ==================================================================================

CREATE TRIGGER ConferenceDates on Conference
FOR INSERT
AS
	IF (select DATEDIFF(DAY,StartDate,EndDate) from inserted) < 0
	BEGIN
		RAISERROR('Data początku konferencji musi być wcześniejsza niż data jej zakończenia!',16,1)
		ROLLBACK TRANSACTION
	END
GO

-- ==================================================================================
-- Trigger pilnujacy aby uczestnik chcący zapisać się na warsztat miał do tego prawo
-- ==================================================================================

CREATE TRIGGER WorkshopDay on WorkshopReservation
FOR INSERT
AS
	IF NOT EXISTS (select DayReservation.DayReservationID from DayReservation
				inner join inserted on DayReservation.DayReservationID = inserted.DayReservationID
				inner join Workshop on inserted.WorkshopID = Workshop.WorkshopID 
				where DayReservation.ConferenceDayID = Workshop.ConferenceDayID)
		BEGIN
			RAISERROR('Nie mozesz zapisac sie na ten warsztat! Nie masz rezerwacji na ten dzien konferencji',15,1)
			ROLLBACK TRANSACTION
		END
GO

-- ==================================================================================
-- Trigger pilnujacy aby stopnie opłat nie wychodziły poza datę rozpoczęcia konferencji
-- ==================================================================================

CREATE TRIGGER PricingLevels on Pricing
FOR INSERT
AS
	IF (select DATEDIFF(DAY,inserted.Deadline,Conference.StartDate) from inserted 
		inner join Conference on Conference.ConferenceID = inserted.ConferenceID) < 0
		BEGIN
			RAISERROR('Deadline progu cenowego nie moze byc okreslony po dacie rozpoczecia konferencji',14,1)
			ROLLBACK TRANSACTION
		END
GO

-- ===================================================================================
-- Trigger pilnujacy aby nie mozna bylo się zapisać na konferencje jesli nie ma miejsc
-- ===================================================================================

CREATE TRIGGER ConfDaySpots on OrderDetails
FOR INSERT
AS
	IF (select dbo.ConferenceDayFreeSpots(inserted.ConferenceDayID)-inserted.spots from inserted) < 0
		BEGIN
			RAISERROR('Nie mozna zarezerwowac poniewac nie ma wystarczajacej liczby miejsc',15,1)
			ROLLBACK TRANSACTION
		END
GO

-- =============================================================================================
-- Trigger pilnujacy aby godzina rozpoczecia warsztatu byla wczesniejsza niz godzina zakonczenia
-- =============================================================================================

CREATE TRIGGER WorkshopHours on Workshop
FOR INSERT
AS
	IF (select DATEDIFF(MINUTE,StartHour,EndHour) from inserted) < 0
	BEGIN
		RAISERROR('Data początku konferencji musi być wcześniejsza niż data jej zakończenia!',16,1)
		ROLLBACK TRANSACTION
	END
GO